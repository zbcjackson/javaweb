package com.odde.web;

import com.odde.web.models.Article;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    @RequestMapping(value="/")
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView("home/index");

        List<Article> articles = new ArrayList<Article>();
        articles.add(new Article("Article A"));

        mav.addObject("articles", articles);
        mav.addObject("article", new Article("Article B"));
        return mav;
    }
}
